<?php
  class User_model extends CI_Model {

    function __construct(){
      parent::__construct();
    }

    // 별명 중복 확인
    public function chk_nickname($nickname){
      $data = $this->db->get_where('user', array('nickname'=>$nickname))->row();
      // SELECT * FROM `user` WHERE `nickname` = $nickname;
      if(empty($data)){
        // echo '비어있음';
        return true;
      }else{
        // echo '값이 존재';
        return false;
      }
    }

    // 추천 가능 확인
    public function chk_recommend($nickname){
      $data = $this->db->get_where('user', array('recommend'=>$nickname))->result_array();
      // SELECT * FROM `user` WHERE `recommend` = $nickname;
      if(count($data)<5){
        //  추천가능
        return true;
      }else{
        //  추천불가
        return false;
      }
    }

    public function create($sql_data){
      $data = $this->db->insert('user', $sql_data);
      // INSERT INTO user (nickname, name, password, phone, email, gender, recommend) VALUES ('김닌나', 'lululu', 'asdfsf', '01055558888', 'nia@naver.com','female', 'hanyoonn')
      // echo "등록완료";
    }

    public function update($nickname, $sql_data){
      // $this->db->where('nickname', $nickname);
      $this->db->update('user', $sql_data, array('nickname' => $nickname));
      // UPDATE `user` SET name = $name, password = $password, phone = $phone, email = $email, gender = $gender WHERE `nickname` = $nickname;
      // echo "수정완료";
    }

    public function delete($nickname){
      $this->db->delete('user', array('nickname' => $nickname));
      // DELETE FROM `user` WHERE `nickname` = $nickname
      // echo "삭제완료";
    }

    public function read($id){
      return $this->db->get_where('user', array('id'=>$id))->row_array();
      // SELECT * FROM `user` WHERE `id` = $id;
    }

    public function read_all(){
      return $this->db->get('user')->result_array();
      // SELECT * FROM `user`;
    }

    public function reads($page){
      $offset = ($page-1)*5;
      return $this->db->get('user', 5, $offset)->result_array();
      // SELECT * FROM `user` LIMIT 5 OFFSET $offset;
    }

  }
?>
