<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('user_model');
		}

		public function index()
		{
			echo "시작이다";
		}

		// 생성 POST
		public function create()
		{
			// 별명
			if(isset($_POST['nickname'])){
				$nickname = $_POST['nickname'];
				$chk_nickname_valid = $this->user_model->chk_nickname($nickname);
				$chk_nickname = preg_match('/^[a-z]{1,30}$/', $nickname);
				if($chk_nickname_valid){
					if(!$chk_nickname){
						$result['error'][] = "별명이 소문자가 아니거나 30자를 넘었습니다.";
					}else{
						$sql_data['nickname'] = $nickname;
					}
				}else{
					$result['error'][] = "별명이 이미 존재합니다.";
				}
			}else{
				$result['error'][] = "별명이 없습니다.";
			}
			// 비밀번호
			if(isset($_POST['password'])){
				$password = $_POST['password'];
				$chk_password = preg_match('/^(?=.*\d{1,50})(?=.*[~`!@#$%\^&*()-+=]{1,50})(?=.*[a-z]{1,50})(?=.*[A-Z]{1,50}).{10,50}$/', $password);
				if(!$chk_password){
					$result['error'][] = "비밀번호가 유효하지 않습니다.";
				}else{
					$sql_data['password'] = $password;
				}
			}else{
				$result['error'][] = "비밀번호가 없습니다.";
			}
			// 이름
			if(isset($_POST['name'])){
				$name = $_POST['name'];
				$chk_name = preg_match('/[\x80-\xFFa-zA-Z]{1,20}/', $name);
				if(!$chk_password){
					$result['error'][] = "이름이 유효하지 않습니다.";
				}else{
					$sql_data['name'] = $name;
				}
			}else{
				$result['error'][] = "이름이 없습니다.";
			}
			// 전화번호
			if(isset($_POST['phone'])){
				$phone = $_POST['phone'];
				$chk_phone = preg_match('/^[0-9]{10,20}$/', $phone);
				if(!$chk_phone){
					$result['error'][] = "전화번호가 유효하지 않습니다.";
				}else{
					$sql_data['phone'] = $phone;
				}
			}else{
				$result['error'][] = "전화번호가 없습니다.";
			}
			// 이메일
			if(Isset($_POST['email'])){
				$email = $_POST['email'];
				$chk_email = preg_match('/^[_\.0-9a-zA-Z-]+@[0-9a-zA-Z][0-9a-zA-Z-]+\.[a-zA-Z]{2,6}$/', $email);
				if(!$chk_email){
					$result['error'][] = "이메일이 유효하지 않습니다.";
				}else{
					$sql_data['email'] = $email;
				}
			}else{
				$result['error'][] = "이메일이 없습니다.";
			}
			// 성별
			if(isset($_POST['gender'])){
				$gender = $_POST['gender'];
				$gender = strtolower($gender);
				$chk_gender = preg_match('/^(male|female)$/', $gender);
				if(!$chk_gender){
					$result['error'][] = "성별이 유효하지 않습니다.";
				}else{
					$sql_data['gender'] = $gender;
				}
			}
			// 추천인
			if(isset($_POST['recommend'])){
				$recommend = $_POST['recommend'];
				$chk_nickname = preg_match('/^[a-z]{1,30}$/', $recommend);
				if($chk_nickname){
					$chk_recommend_valid = $this->user_model->chk_recommend($recommend);
					if(!$chk_recommend_valid){
						$result['error'][] = "추천인원이 5명을 넘었습니다.";
					}else{
						$sql_data['recommend'] = $recommend;
					}
				}else{
					$result['error'][] = "추천인 입력이 유효하지 않습니다.";
				}
			}


			// 저장
			if(isset($result['error'])){
				$result['status'] = 'error';
			}else{
				$this->user_model->create($sql_data);
				$result['status'] = 'success';
			}
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
		}


		// 수정 PUT
		public function update()
		{
			$_PUT = json_decode(file_get_contents('php://input'), true);
			// print_r($_PUT['name']);

			// 별명
			if(isset($_PUT['nickname'])){
				$nickname = $_PUT['nickname'];
				$chk_nickname = preg_match('/^[a-z]{1,30}$/', $nickname);
				$chk_nickname_valid = $this->user_model->chk_nickname($nickname);
				if($chk_nickname){
					if($chk_nickname_valid){
						$result['error'][] = "일치하는 별명이 없습니다.";
					}
				}else{
					$result['error'][] = "별명이 유효하지 않습니다.";
				}
			}else{
				$result['error'][] = "별명이 없습니다.";
			}

			// 비밀번호
			if(isset($_PUT['password'])){
				$password = $_PUT['password'];
				$chk_password = preg_match('/^(?=.*\d{1,50})(?=.*[~`!@#$%\^&*()-+=]{1,50})(?=.*[a-z]{1,50})(?=.*[A-Z]{1,50}).{10,50}$/', $password);
				if(!$chk_password){
					$result['error'][] = "비밀번호가 유효하지 않습니다.";
				}else{
					$sql_data['password'] = $password;
				}
			}

			// 이름
			if(isset($_PUT['name'])){
				$name = $_PUT['name'];
				$chk_name = preg_match('/[\x80-\xFFa-zA-Z]{1,20}/', $name);
				if(!$chk_name){
					$result['error'][] = "이름이 유효하지 않습니다.";
				}else{
					$sql_data['name'] = $name;
				}
			}

			// 전화번호
			if(isset($_PUT['phone'])){
				$phone = $_PUT['phone'];
				$chk_phone = preg_match('/^[0-9]{10,20}$/', $phone);
				if(!$chk_phone){
					$result['error'][] = "전화번호가 유효하지 않습니다.";
				}else{
					$sql_data['phone'] = $phone;
				}
			}

			// 이메일
			if(Isset($_PUT['email'])){
				$email = $_PUT['email'];
				$chk_email = preg_match('/^[_\.0-9a-zA-Z-]+@[0-9a-zA-Z][0-9a-zA-Z-]+\.[a-zA-Z]{2,6}$/', $email);
				if(!$chk_email){
					$result['error'][] = "이메일이 유효하지 않습니다.";
				}else{
					$sql_data['email'] = $email;
				}
			}

			// 성별
			if(isset($_PUT['gender'])){
				$gender = $_PUT['gender'];
				$gender = strtolower($gender);
				$chk_gender = preg_match('/^(male|female)$/', $gender);
				if(!$chk_gender){
					$result['error'][] = "성별이 유효하지 않습니다.";
				}else{
					$sql_data['gender'] = $gender;
				}
			}


			// 저장
			if(isset($result['error'])) {
				$result['status'] = 'error';
			}else{
				$this->user_model->update($nickname, $sql_data);
				$result['status'] = 'success';
			}
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
		}

		// 삭제 DELETE
		public function delete()
		{
			$_DELETE = json_decode(file_get_contents('php://input'), true);
			if(isset($_DELETE['nickname'])){
				$nickname = $_DELETE['nickname'];
				$chk_nickname = $this->user_model->chk_nickname($nickname);
				if($chk_nickname){
					$result['error'][] = "일치하는 별명이 없습니다.";
				}
			}else{
				$result['error'][] = "별명이 없습니다.";
			}

			// 실행
			if(isset($result['error'])) {
				$result['status'] = 'error';
			}else{
				$this->user_model-> delete($nickname);
				$result['status'] = 'success';
			}
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
		}

		// 회원 프로필($id) GET
		public function read($id)
		{
			// echo "유저".$id;
			$result = $this->user_model->read($id);
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
		}
		// 모든 데이타 리스트 GET
		public function read_all()
		{
			// echo "유져리스트";
			$result = $this->user_model->read_all();
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
		}
		// 데이타 리스트($page) GET
		public function reads($page)
		{
			// echo "유져 페이지";
			$result = $this->user_model->reads($page);
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
		}
}
