-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- 생성 시간: 20-03-04 01:47
-- 서버 버전: 5.7.29-log
-- PHP 버전: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 데이터베이스: `idus`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'id',
  `name` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '이름',
  `nickname` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '별명',
  `password` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '비밀번호',
  `phone` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '전화번호',
  `email` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '이메일',
  `gender` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '성별',
  `recommend` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '추천인',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '생성시간',
  `update_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '수정시간'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 테이블의 덤프 데이터 `user`
--

INSERT INTO `user` (`id`, `name`, `nickname`, `password`, `phone`, `email`, `gender`, `recommend`, `created_at`, `update_at`) VALUES
(1, '홍길동', 'hong', '9sNhG@APmUT5I', '8711765390', 'hong@naver.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(2, '이순신', 'lee', 'dfSNa9@jjev', '6159677383', 'lee@naver.com', 'Male', 'hong', '2020-03-04 01:35:31', '2020-03-04 01:36:45'),
(3, '김나나', 'kimnana', 'Sk3C@Jb6K', '2673244494', 'kimnana@naver.com', 'Male', 'hong', '2020-03-04 01:35:31', '2020-03-04 01:37:03'),
(4, '아무개', 'amuge', 'ITWgu3@sKv', '6574432201', 'amuge@naver.com', 'Female', 'hong', '2020-03-04 01:35:31', '2020-03-04 01:36:57'),
(5, '주두혜', 'joo', 'eV@PR5H50sx', '6453145592', 'joo@hanmail.net', 'Male', 'hong', '2020-03-04 01:35:31', '2020-03-04 01:37:33'),
(6, '연지준', 'yeonji', 'XFYWeg@as5s', '7611313592', 'yeonji@hanmail.net', 'Male', 'hong', '2020-03-04 01:35:31', '2020-03-04 01:37:40'),
(7, '송이철', 'ssong', 'bUIDfd@0yDN8', '7501472060', 'ssong@hanmail.net', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(8, '박조윤', 'parkjo', '2rj9@uyuF1mO', '3278862633', 'parkjo@hanmail.net', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(9, '김보광', 'kimbo', '1fN2YP@Mb4J1', '9808092584', 'kimbo@gmail.com', 'Female', 'jera', '2020-03-04 01:35:31', '2020-03-04 01:38:48'),
(10, '정도균', 'jung', 'Wfv0d@OHGO', '6581125998', 'jung@gmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(11, 'Cindee', 'cindee', 'eXSgVx@@jOj2r', '7356765730', 'cindee@hotmail.com', 'Female', 'jera', '2020-03-04 01:35:31', '2020-03-04 01:38:18'),
(12, 'Waylen', 'waylen', 'NmOaBN@4Q55pq', '4346988037', 'waylen@hotmail.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(13, 'Dita', 'dita', 'Fryp@34TmxG', '3511197935', 'dita@hotmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(14, 'Ashia', 'ashia', 'uAmE1q@wW', '8834105371', 'ashia@hotmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(15, 'Michal', 'michal', 'g0i@6ATH', '1093109954', 'michal@hotmail.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(16, 'Silvio', 'silvio', 'QL@5xB8N', '3511673752', 'silvio@hotmail.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(17, 'Bald', 'bald', '9kL@HasdzV', '6716480910', 'bald@hotmail.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(18, 'Gearalt', 'gearalt', 's@Kasd6xwH', '8755019771', 'gearalt@gmail.com', 'Male', 'kevin', '2020-03-04 01:35:31', '2020-03-04 01:39:14'),
(19, 'Kevin', 'kevin', 'dBI@BasdadOF', '5545722182', 'kevin@gmail.com', 'Male', 'kevin', '2020-03-04 01:35:31', '2020-03-04 01:39:03'),
(20, 'Jeri', 'jera', 'FsXXGq@vENy', '9406354560', 'jera@gmail.com', 'Female', 'kevin', '2020-03-04 01:35:31', '2020-03-04 01:38:58'),
(21, 'Whitney', 'whitney', 'mtUas@nxeNnm8fl', '2197262420', 'whitne@gmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(22, 'Ingaborg', 'ingaborg', 'Ib@asdsf1FNHH', '6982810703', 'ingaborg@gmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(23, 'Bryana', 'bryana', 'cQusfasf$240L', '6402686806', 'bryana@gmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(24, 'Amerigo', 'amergio', 'Q@w2grc4v2WcY', '9784177821', 'amergio@gmail.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(25, 'Nesta', 'nesta', 'rDFwnQ#3nNSg', '3284416632', 'nesta@gmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(26, 'Brand', 'brand', 'Pm6F#sa2ELG', '2729067746', 'brand@gmail.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(27, 'Evyn', 'evyn', 'HWDWCzg@2uDm', '7857616039', 'evyn@gmail.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(28, 'Anett', 'anett', 'sMy8CD@3HwzS', '1936113551', 'anett@gmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(29, 'Elna', 'elna', 'qF3XTG7@Qahy8', '1755559400', 'elna@gmail.com', 'Female', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(30, 'Vachel', 'vachel', 'VX4z@as59zW', '3913741108', 'vachel@gmail.com', 'Male', '', '2020-03-04 01:35:31', '2020-03-04 01:35:31'),
(31, 'frank', 'frank', 'qweASD123$', '01090331108', 'frank@naver.com', 'male', 'jera', '2020-03-04 01:45:40', '2020-03-04 01:45:40');

--
-- 덤프된 테이블의 인덱스
--

--
-- 테이블의 인덱스 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 덤프된 테이블의 AUTO_INCREMENT
--

--
-- 테이블의 AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
